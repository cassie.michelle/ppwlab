from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Course
from .forms import Course_Form

# Create your views here.
response = {'author': "Cassie Michelle"}

def home(request) :
    return render(request, 'home.html')

def contact(request) :
    return render(request, 'contact.html')

def guestbook(request) :
    return render(request, 'guestbook.html')


def index(request) :
    html = 'jadwal.html'
    response['course_form'] = Course_Form
    return render(request, html, response)

def post_form(request):
    # courses = Course.objects.all().values()
    # form = Course(request.POST or None)
    if(request.method == 'POST'):
        response['name'] = request.POST['course_name']
        response['category'] = request.POST['course_category']
        response['place'] = request.POST['course_place']
        response['time'] = request.POST['course_time']
        print(response['name'])
        result = Course(course_name=response['name'], course_category=response['category'], course_place=response['place'], course_time=response['time'])
        result.save()

        html ='jadwal.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/story4/')

def table_form(request):
    courses = Course.objects.all()
    response['courses'] = courses
    html = 'tbl_jadwal.html'
    return render(request, html , response)

def delete_form(request):
    courses = Course.objects.all().delete()
    response['courses'] = courses
    return render(request,'tbl_jadwal.html', response )




