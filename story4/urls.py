from django.conf.urls import url
from .views import home, contact, guestbook, index, post_form,table_form, delete_form

urlpatterns = [
    url('home', home),
    url('contact', contact),
    url('guestbook', guestbook),
    url('add_form',post_form,name='add_form'),
    url('delete_form',delete_form,name='delete_form'),
    url('table_form', table_form, name='table_form'),
    url('index', index),
    url('', home)
    # url('post/new', views.post_new, name='post_new'),

]