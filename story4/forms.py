from django import forms

# from .models import Post

# class PostForm(forms.ModelForm):

#     class Meta:
#         model = Post
#         fields = ('title', 'text',)

class Course_Form(forms.Form):
    # error_messages = {
    # 'required': 'Tolong isi input ini',
    # 'invalid': 'Isi input dengan email',
    # }
    attrs = {'class': 'form-control'}

    course_name = forms.CharField(label='Course Name', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    course_category = forms.CharField(label='Course Category', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    course_place = forms.CharField(widget=forms.Textarea(attrs={'rows':4, 'cols':15, 'class': 'form-control' }),required=True)
    course_time = forms.DateTimeField(label ='Course Time', required=True , widget=forms.DateTimeInput(attrs={'type':'datetime-local','class': 'form-control'}))
    