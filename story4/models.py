from django.db import models
from django.utils import timezone

# Create your models here.

class Course(models.Model):
    # published_date = models.DateTimeField(default=timezone.now)
    course_name = models.CharField(max_length=30)
    course_category = models.CharField(max_length=30)
    course_place = models.TextField()
    course_time = models.DateTimeField()
    

# class Post(models.Model):
#     author = models.ForeignKey(Course, on_delete = models.CASCADE)
#     published_date = models.DateTimeField(default=timezone.now)


